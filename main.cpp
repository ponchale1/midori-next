#include <QGuiApplication>
#include <QQmlApplicationEngine>
// XXX: #include <QtQuickControls2/QQuickStyle>
#include <QIcon>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

#if defined(Q_OS_WIN)
    const QString theme("Universal");
#else
    const QString theme("Material");
#endif
    qputenv("QT_QUICK_CONTROLS_STYLE", theme.toUtf8());
    qputenv("QT_QUICK_CONTROLS_UNIVERSAL_THEME", "Dark");
    qputenv("QT_QUICK_CONTROLS_UNIVERSAL_ACCENT", "Green");
    qputenv("QT_QUICK_CONTROLS_MATERIAL_THEME", "Dark");
    qputenv("QT_QUICK_CONTROLS_MATERIAL_ACCENT", "LightGreen");

    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("org.midori-browser.midori"),
                                       QIcon::fromTheme(QStringLiteral("web-browser"))));

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
